El cinema ABC tiene 2 salas, la primera con un aforo de 20 sillas general y 10 preferencial  y la segunda con un aforo de 25 sillas generales. 

Se desea una aplicación que administre el valor de venta de boletas para las funciones de las 3 y 6 pm teniendo en cuenta lo siguiente:

1. La boleta general vale 8500

2. La boleta preferencial vale 10000

3. Si compra para la función de 3:00 pm y compra más de 2 boletas se disminuye por boleta un valor de 400 pesos en general y 800 en preferencial. 

4. Si compra para la función de las 6:00 pm y compra mas de 2 boletas se disminuye en 300 y 700 pesos respectivamente según la localidad.

5. Si compra boleta para una función diferente al día actual tiene un recargo de 300 por boleta  a modo de reserva. 

6. Por último si esta dentro de las ultimas 10 boletas del teatro tendrá un descuento de 1000 pesos adicionales sin importar la localidad. 

Se desea saber el valor del recaudo por función en cada sala. Y total de cinema.

- sala 1 3:00 pm y 6:00 pm

- sala 2 3:00 pm y 6:00 pm

Total. 
adicional: Base de datos / GUI ventana.