/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Juan
 * Created: 31/08/2021
 */

CREATE DATABASE `CINEMA_ABC`;
CREATE USER 'Admin' IDENTIFIED BY 'clave';
GRANT USAGE ON *.* TO 'Admin'@'%' IDENTIFIED BY 'clave';
GRANT ALL privileges ON `CINEMA_ABC`.* TO 'Admin'@'%';

-- Tabla de costos para general (1) y preferencial (2)

CREATE TABLE tbl_costos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    valor Float NOT NULL
);

CREATE TABLE tbl_salas (
    id        INT AUTO_INCREMENT PRIMARY KEY, -- El id deberia ser solo de una fila osea siempre deberia ser 1 
    tbl_sala1 INT NOT NULL,
    tbl_sala2 INT NOT NULL
    FOREIGN KEY (tbl_sala1) REFERENCES tbl_sala1(id),
    FOREIGN KEY (tbl_sala2) REFERENCES tbl_sala2(id)
)

CREATE TABLE tbl_sala1 (
    id     INT AUTO_INCREMENT PRIMARY KEY,
    sillas INT
)

CREATE TABLE tbl_sala2 (
    id     INT AUTO_INCREMENT PRIMARY KEY,
    sillas INT
)

CREATE TABLE tbl_clientes (
    id        INT AUTO_INCREMENT PRIMARY KEY,
    nombre    VARCHAR(100),
    apellido  VARCHAR(100),
    documento BIGINT
);

CREATE TABLE tbl_peliculas (
    id     INT AUTO_INCREMENT PRIMARY KEY,
    nombre   VARCHAR(100),
    duracion TIME,
    hora   TIME,
    sala   INT,
    FOREIGN KEY (sala) REFERENCES tbl_salas(id)
);

CREATE TABLE tbl_descuentos ( -- Tabla descuentos debe ser solo con el valor de los descuentos
    id    INT AUTO_INCREMENT PRIMARY KEY,
    valor FLOAT NOT NULL,
    
);

