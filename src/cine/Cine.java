package cine;

import funcion.Funcion;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import persistencia.Persistencia;
import sala.Sala;
import silla.Silla;

public class Cine {
    
    private Map<String, Funcion> funciones;

    public Cine() {
        
        Persistencia leer = new Persistencia();
        this.funciones = leer.abrirArchivo();
        
        if (this.funciones == null) {

            this.funciones = new HashMap<String, Funcion>();

        }

    }
    
    public boolean adicionarFuncion(Funcion funcion){
        
        String nombre = funcion.getPelicula();
        
        Persistencia addFuncion = new Persistencia();
        
        if (funcion.isTrespm()){
            nombre += "3";
            
        } else {
            nombre += "6";
            
        }
        
        this.funciones.put(nombre, funcion);
        
        addFuncion.guardar(funciones);
        
        return true;
        
    }
    
    public ArrayList<String> boletasDisponiblesFuncion(Funcion funcion){
        
        return funcion.getSala().disponibles();
        
    }
    
    public ArrayList<String> funcionesDisponibles(){
        
        Set<String> funcionnombre = this.funciones.keySet();
        ArrayList<String> codigos = new ArrayList<>(funcionnombre);
        
        return codigos;
    } 
    
    public Funcion getFuncion(String nombre){

        Funcion ifuncion = funciones.get(nombre);
        
        return ifuncion;

    } 
    
    public boolean comprarBoleta(String funcion, ArrayList<Silla> sillas){
        
        Funcion lafuncion = this.funciones.get(funcion);
        
        if (lafuncion != null){
            
            for (Silla silla : sillas) {
                
                //silla.setTrespm(lafuncion.isTrespm());
                lafuncion.getSala().asignarSilla(silla.getCodigo(), silla);
                
            }
            
            return true;
            
        }
        
        return false;
        
    }
    
    public double valorXfuncion(String funcion){
        
        double rta = this.funciones.get(funcion) != null ? this.funciones.get(funcion).calculaRecaudo():0;
        
        return rta;
        
    }
    
        public double valorXventa(String funcion, ArrayList<Silla> sillas){

        double rta = this.funciones.get(funcion) != null ? this.funciones.get(funcion).calculaVenta(sillas):0;
        
        return rta;
        
    }
    
    public static void main(String[] args) {
        Cine c = new Cine();
        
        Funcion f = new Funcion("TERMINA-HECTOR", "ACCION", true);
        Sala sala = new Sala(0, 25);
        f.setSala(sala);
        
        c.adicionarFuncion(f);
        
        
        ArrayList<String> dispoArrayList= c.boletasDisponiblesFuncion(f);
        
        for (String boletas : dispoArrayList) {
            System.out.println("BOLETAS "+boletas);
        }

        dispoArrayList= c.funcionesDisponibles();
        
        for (String funciones : dispoArrayList) {
            System.out.println("FUNCIONES ->"+funciones);
        }
        
        
        
        Silla s = new Silla(2);
        s.setCodigo("GEN_S4");
        s.setMasdos(true);
        s.setReserva(false);
        s.setUltimas10(true);
        
        Silla s1 = new Silla(2);
        s1.setCodigo("GEN_S5");
        s1.setMasdos(true);
        s1.setReserva(false);
        s1.setUltimas10(true);

        Silla s2 = new Silla(2);
        s2.setCodigo("GEN_S6");
        s2.setMasdos(true);
        s2.setReserva(false);
        s2.setUltimas10(true);
        
        ArrayList<Silla> silla = new ArrayList();
        silla.add(s);
        silla.add(s1);
        silla.add(s2);
        
        c.comprarBoleta("TERMINA-HECTOR3", silla);
        
        
        dispoArrayList= c.boletasDisponiblesFuncion(f);
        
        for (String boletas : dispoArrayList) {
            System.out.println("BOLETAS 2 "+boletas);
        }
        System.out.println(c.valorXfuncion("TERMINA-HECTOR3"));
                
    }
    // Buscar ordenado 
    
}