/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import funcion.Funcion;
import java.io.FileNotFoundException;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Persistencia {
    
    private final String NOMBREARCHIVO    = "funciones.asi";
    private final String NOMBREARCHIVOLOG = "logRecorder.log";

    public boolean guardar(Map<String, Funcion> lstFuncion) {

        FileOutputStream fileOpen = null;

        try {

            fileOpen = new FileOutputStream(NOMBREARCHIVO);

            ObjectOutputStream objectOpen = new ObjectOutputStream(fileOpen);

            objectOpen.writeObject(lstFuncion);           

            objectOpen.close();

            return true;

        } catch(FileNotFoundException fnfe){
            
            guardarLog(fnfe.toString());

        } catch (IOException ioe) {

            guardarLog(ioe.toString());

        } finally {
            try {
                
                fileOpen.close();

            } catch (IOException ioe) {

                guardarLog(ioe.toString());
            }
        }

        return false;
    }

    public Map<String, Funcion> abrirArchivo() {

        Map<String, Funcion> FuncionesArchivo = null;
        FileInputStream fileOpen = null;

        try {

            fileOpen = new FileInputStream(NOMBREARCHIVO);
            
            ObjectInputStream objectOpen = new ObjectInputStream(fileOpen);
            
            //System.out.println(objectOpen.readObject());
            FuncionesArchivo = (HashMap<String, Funcion>)objectOpen.readObject();
            
            //se cierra archivo

            objectOpen.close();
            fileOpen.close();

            return FuncionesArchivo;

        } catch (FileNotFoundException fnfe) {

            guardarLog(fnfe.toString());


        } catch (IOException ioe) {

            guardarLog(ioe.toString());


        } catch (ClassNotFoundException cnfe) {

            guardarLog(cnfe.toString());

        }

        return null;
    }

    public boolean guardarLog(String err) {

        FileOutputStream fileOpen = null;

        try {

            fileOpen = new FileOutputStream(NOMBREARCHIVOLOG);

            ObjectOutputStream objectOpen = new ObjectOutputStream(fileOpen);
            objectOpen.writeObject(err);

            objectOpen.close();

            return true;

        } catch(FileNotFoundException fnfe){
            
            guardarLog(fnfe.toString());

        } catch (IOException ioe) {

            guardarLog(ioe.toString());

        } finally {
            try {
                
                fileOpen.close();

            } catch (IOException ioe) {

                guardarLog(ioe.toString());
            }
        }

        return false;
        
    }
 
}