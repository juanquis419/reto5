package dataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConnectionDB {
    
    private final String DBNAME = "CINEMA_ABC"; // Deberian ser variables de entorno por seguridad
    private final String USER   = "Admin";
    private final String PASSWD = "clave";
    
    public Connection connection(){
        
        try {
            
            Class.forName("org.mariadb.jdbc.Driver");
            Connection conexion = DriverManager.getConnection("jdbc:mariadb://localhost:3306/" + this.DBNAME, USER, PASSWD);
            
            return conexion;
            
        } catch (ClassNotFoundException | SQLException ex) {
            
            Logger.getLogger(ConnectionDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public void cerrarConnection (Connection conex){
        
        try {
            conex.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionDB.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        
    }
    
}

