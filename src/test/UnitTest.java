/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.sql.Connection;
import org.junit.Test;
import dataBase.ConnectionDB;
import org.junit.Assert;

public class UnitTest {

    /**
     * @param args the command line arguments
     */
    
    @Test
    public void pruebaConexion(){
        ConnectionDB mcon = new ConnectionDB();
        Connection cActivo = mcon.connection();
        
        Assert.assertNotNull(cActivo);
    }
    
}
