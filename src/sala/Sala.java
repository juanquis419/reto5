package sala;

import silla.Silla;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Sala {
    
    private Map <String, Silla> sillasGEN ;
    private Map <String, Silla> sillasPRE ;
    
    private int tamsillasPRE;
    private int tamsillasGEN;

    public Sala(int tamsillasPRE, int tamsillasGEN) {
        
        this.tamsillasPRE = tamsillasPRE;
        this.tamsillasGEN = tamsillasGEN;
        
        sillasGEN         = new HashMap<>();
        sillasPRE         = new HashMap<>();
        
        String codigo     = "";
        
        for (int i = 0; i < (tamsillasPRE); i++) {
            codigo = "PRE_S" + (i+1);
            sillasPRE.put(codigo, null);
            
        }
        
        for (int i = 0; i < tamsillasGEN; i++) {
            codigo = "GEN_S" + (i+1);
            sillasGEN.put(codigo, null);
            
        }
        
    }

    public int getTamsillasPRE() {
        return tamsillasPRE;
    }

    public int getTamsillasGEN() {
        return tamsillasGEN;
    }  
    
    public boolean asignarSilla(String codSilla, Silla laSilla){
        
        if (codSilla.contains("GEN")){
            
            return asignarSillaGen(codSilla, laSilla);
            
        }else{
            
            return asignarSillaPre(codSilla, laSilla);
            
        }
        
    }
    
    private boolean asignarSillaGen (String codSilla, Silla lasilla){
        
        if (this.sillasGEN.get(codSilla) == null){
            
            this.sillasGEN.put(codSilla, lasilla);
            return true;
            
        }
        
        return false;
        
    }
    
    private boolean asignarSillaPre (String codSilla, Silla laSilla){
        
        if (this.sillasPRE.get(codSilla) == null){
            
            this.sillasPRE.put(codSilla, laSilla);
            return true;
            
        }
        
        return false;
        
    }
    
    public ArrayList<String> disponibles(){
        
        ArrayList<String> codigos = new ArrayList<>();
        Set<String> codigoSilla = this.sillasGEN.keySet();
        
        for (String cod : codigoSilla) {
            
            if (this.sillasGEN.get(cod) == null){
                
                codigos.add(cod);
                
            }
            
        }
        
        codigoSilla = this.sillasPRE.keySet();
        
        for (String cod : codigoSilla) {
            
            if (this.sillasPRE.get(cod) == null){
                
                codigos.add(cod);
            }
            
        }
        
        return codigos;
        
    }
    
    public ArrayList<String> noDisponibles(){
        
        ArrayList<String> codigos = new ArrayList<>();
        Set<String> codigoSilla = this.sillasGEN.keySet();
        
        for (String cod : codigoSilla) {
            
            if (this.sillasGEN.get(cod) != null){
                
                codigos.add(cod);
                
            }
            
        }
        
        codigoSilla = this.sillasPRE.keySet();
        
        for (String cod : codigoSilla) {
            
            if (this.sillasPRE.get(cod) != null){
                
                codigos.add(cod);
            }
            
        }

        return codigos;
        
        
    }
    
    public ArrayList<String> ocupadosPRE(){
        
        ArrayList<String> codigos = new ArrayList<>();
        Set<String> codigoSilla   = this.sillasPRE.keySet();
        codigoSilla               = this.sillasPRE.keySet();
        
        for (String cod : codigoSilla) {
            
            if (this.sillasPRE.get(cod) != null){
                
                codigos.add(cod);
                
            }
            
        }
        
        return codigos;
        
    }
    
    
    
    public ArrayList<String> ocupadosGEN(){
        
        ArrayList<String> codigos = new ArrayList<>();
        Set<String> codigoSilla   = this.sillasGEN.keySet();
        
        for (String cod : codigoSilla) {
            
            if (this.sillasGEN.get(cod) != null){
                
                codigos.add(cod);
                
            }
            
        }
        
        return codigos;
    }
    
    public double calculaRecaudo(){
        
        double totales           = 0;
        ArrayList<String> llaves = this.ocupadosPRE();
        
        for (String llave : llaves) {
            
            totales +=this.sillasPRE.get(llave).valorPagar();
            
        }
        
        llaves = this.ocupadosGEN();
        
        for (String llave : llaves) {
            
            totales +=this.sillasGEN.get(llave).valorPagar();
            
        }
        
        return totales;
        
    }
    
        public double calculaVenta(ArrayList<Silla> sillas){
        
        double total = 0;
        
        for (Silla isilla : sillas) { // traer nombre objeto, sino error

            if (isilla.getCodigo().substring(0,3).equals("PRE")){
                
                total +=this.sillasPRE.get(isilla.getCodigo()).valorPagar();
                
            }else {
                
                total += this.sillasGEN.get(isilla.getCodigo()).valorPagar();   
            }
            
        }
        
        return total;
        
    }
    
    
    
    
    // Main no va -----> va en app.java
    public static void main(String[] args) {
        Sala lassalas = new Sala(5, 10);
        System.out.println("FIN");
        ArrayList<String> codigos = lassalas.disponibles();
        for (String codigo : codigos) {
            System.out.println(codigo);
            
        }
        Silla silla = new Silla(1);
        lassalas.asignarSillaGen("GEN_S10", silla);
        lassalas.asignarSillaGen("GEN_S3", silla);
        lassalas.asignarSillaGen("GEN_S6", silla);
        lassalas.asignarSillaPre("PRE_S2", silla);
        lassalas.asignarSillaPre("PRE_S5", silla);
        codigos = lassalas.disponibles();
        System.out.println("- - -- -- - - -- - - ");
        for (String codigo : codigos) {
            System.out.println(codigo);
            
        }
        
    }
    
}
